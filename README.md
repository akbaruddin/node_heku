# node-js-getting-started

Your app should now be running on [localhost:5000](http://localhost:5000/).

## Deploying to Heroku

```
$ heroku create
$ git push heroku master
$ heroku open
```
or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## Documentation

heroku: <https://git.heroku.com/codecamp1991.git>

```bash
yarn start
ngrok http 5000
```