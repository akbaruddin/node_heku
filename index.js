const express = require('express')
const path = require('path')
var fs = require('fs');
require('dotenv').config()

const PORT = process.env.PORT || 5000
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bodyParser = require('body-parser');

const app = express();
mongoose.connect(process.env.MONGO_URI, {
  useUnifiedTopology: true
});

app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

if (!process.env.DISABLE_XORIGIN) {
  app.use(function(req, res, next) {
    var allowedOrigins = ['https://narrow-plane.gomix.me', 'https://www.freecodecamp.com'];
    var origin = req.headers.origin || '*';
    if(!process.env.XORIG_RESTRICT || allowedOrigins.indexOf(origin) > -1){
      console.log(origin);
      res.set({
        "Access-Control-Allow-Origin" : origin,
        "Access-Control-Allow-Methods" : "GET, POST, OPTIONS",
        "Access-Control-Allow-Headers" : "Origin, X-Requested-With, Content-Type, Accept"
      });

      if ('OPTIONS' === req.method) {
        //respond with 200
        res.send(200);
      }
    }
    next();
  });
}

var rawBodySaver = function (req, res, buf, encoding) {
  if (buf && buf.length) {
    req.rawBody = buf.toString(encoding || 'utf8');
  }
}

app.use(bodyParser.json({ verify: rawBodySaver }));
app.use(bodyParser.urlencoded({ verify: rawBodySaver, extended: true }));
app.use(bodyParser.raw({ verify: rawBodySaver, type: function () { return true } }));


// schema
const personSchema = new Schema({
  original_url: String,
  short_url: Number
});

app.get('/', (req, res) => res.render('pages/index'))

app.get('/_api/file/package.json', (req, res) => {
  fs.readFile(__dirname + '/package.json', function(err, data) {
    if(err) return next(err);
    res.type('txt').send(data.toString());
  });
})

app.get('/_api/is-mongoose-ok', (req, res) => {
  if (mongoose) {
    res.json({isMongooseOk: !!mongoose.connection.readyState})
  } else {
    res.json({isMongooseOk: false})
  }
})

app.post('/_api/mongoose-model', (req, res) => {
  const Person = mongoose.model("Person", personSchema);
  var p = new Person(req.body);
  // res.json(p);
  p.save((err, data) => {
    if(err) {
      res.json(err)
    }
    else {
      res.json(data)
    }
  });
})

const Person = mongoose.model("Person", personSchema);


var findPeopleByName = function(personName, done) {
  Person.find({name: personName}, function (err, personFound) {
    if (err) return console.log(err);
    done(null, personFound);
  });
};

app.post('/_api/find-all-by-name', function(req, res, next) {
  console.log(req.body.name)
  findPeopleByName(req.body.name, function(err, data) {
    if(err) { return (next(err)); }

    return res.json(data);
  })
});


var findOneByFood = function(food, done) {
  Person.findOne({favoriteFoods: food}, function (err, data) {
    if (err) return console.log(err);
    done(null, data);
  });
};

app.post('/_api/find-one-by-food', function(req, res, next) {
  console.log(req.body.favoriteFoods)
  findOneByFood(req.body.favoriteFoods, function(err, data) {
    if(err) { return (next(err)); }

    return res.json(data);
  })
});

var findPersonById = function(personId, done) {
  Person.findById(personId, function (err, data) {
    if (err) return console.log(err);
    done(null, data);
  });
};

app.get('/_api/find-by-id', function(req, res, next) {
  var t = setTimeout(() => { next({message: 'timeout'}) }, timeout);
  var p = new Person({name: 'test', age: 0, favoriteFoods: ['none']});
  p.save(function(err, pers) {
    if(err) { return next(err) }
    findPersonById(pers._id, function(err, data) {
      clearTimeout(t);
      if(err) { return next(err) }
      if(!data) {
        console.log('Missing `done()` argument');
        return next({message: 'Missing callback argument'});
      }
      res.json(data);
      p.remove();
    });
  });
});

let findEditThenSave = (personId, done) => {
  let foodToAdd = 'hamburger';
  
  Person.findById(personId, (err, data) => {
    if (err) { 
      done(err) 
    }
    data.favoriteFoods.push(foodToAdd);
    data.save((err, data) => err ? done(err) : done(null, data));
  });
};

app.post('/_api/find-edit-save', function(req, res, next) {
  var t = setTimeout(() => { next({message: 'timeout'}) }, timeout);
  var p = new Person(req.body);
  p.save(function(err, pers) {
    if(err) { return next(err) }
    try {
      findEditThenSave(pers._id, function(err, data) {
        clearTimeout(t);
        if(err) { return next(err) }
        if(!data) {
          console.log('Missing `done()` argument');
          return next({message: 'Missing callback argument'});
        }
        res.json(data);
        p.remove();
      });
    } catch (e) {
      console.log(e);
      return next(e);
    }
  });
});

var findAndUpdate = (personName, done) => {
  var ageToSet = 20;

  Person.findOneAndUpdate({name: personName}, {age: ageToSet}, {new: true}, (err, data) => err ? done(err) : done(null, data));
};

app.post('/_api/find-one-update', function(req, res, next) {
  var t = setTimeout(() => { next({message: 'timeout'}) }, timeout);
  var p = new Person(req.body);
  p.save(function(err, pers) {
    if(err) { return next(err) }
    try {
      findAndUpdate(pers.name, function(err, data) {
        clearTimeout(t);
        if(err) { return next(err) }
        if (!data) {
          console.log('Missing `done()` argument');
          return next({ message: 'Missing callback argument' });
        }
        res.json(data);
        p.remove();
      });
    } catch (e) {
      console.log(e);
      return next(e);
    }
  });
});

var removeById = (personId, done) => {
  Person.findByIdAndRemove(personId, (err, data) => err ? done(err) : done(null, data));    
};

app.post('/_api/remove-one-person', function(req, res, next) {
  Person.remove({}, function(err) {
    if(err) if(err) { return next(err) }
    var t = setTimeout(() => { next({message: 'timeout'}) }, timeout);
    var p = new Person(req.body);
    p.save(function(err, pers) {
      if(err) { return next(err) }
      try {
        removeById(pers._id, function(err, data) {
          clearTimeout(t);
          if(err) { return next(err) }
          if(!data) {
            console.log('Missing `done()` argument');
            return next({message: 'Missing callback argument'});
          }
          console.log(data)
          Person.count(function(err, cnt) {
            if(err) { return next(err) }
            data = data.toObject();
            data.count = cnt;
            console.log(data)
            res.json(data);
          })
        });
      } catch (e) {
        console.log(e);
        return next(e);
      }
    });
  });
});

var removeManyPeople = (done) => {
  var nameToRemove = "Mary";

  Person.remove({name: nameToRemove}, (err, data) => err ? done(err) : done(null, data));
};

app.post('/_api/remove-many-people', function(req, res, next) {
  Person.remove({}, function(err) {
    if(err) { return next(err) }
    var t = setTimeout(() => { next({message: 'timeout'}) }, timeout);
    Person.create(req.body, function(err, pers) {
      if(err) { return next(err) }
      try {
        removeManyPeople(function(err, data) {
          clearTimeout(t);
          if(err) { return next(err) }
          if(!data) {
            console.log('Missing `done()` argument');
            return next({message: 'Missing callback argument'});
          }
          Person.count(function(err, cnt) {
            if(err) { return next(err) }
            if (data.ok === undefined) {
              // for mongoose v4
              try {
                data = JSON.parse(data);
              } catch (e) {
                console.log(e);
                return next(e);
              }
            }
            res.json({
              n: data.n,
              count: cnt,
              ok: data.ok
            });
          })
        });
      } catch (e) {
        console.log(e);
        return next(e);
      }
    });
  })
});

var queryChain = done => {
  var foodToSearch = "burrito";
  
  Person.find({favoriteFoods: foodToSearch})
    .sort({name: 'asc'})
    .limit(2)
    .select('-age')
    .exec((err, data) => err ? done(err) : done(null, data));
};

app.post('/_api/query-tools', function(req, res, next) {
  var t = setTimeout(() => { next({message: 'timeout'}) }, timeout);
  Person.remove({}, function(err) {
    if(err) if(err) { return next(err) }
    Person.create(req.body, function(err, pers) {
      if(err) { return next(err) }
      try {
        queryChain(function(err, data) {
          clearTimeout(t);
          if(err) { return next(err) }
          if (!data) {
            console.log('Missing `done()` argument');
            return next({ message: 'Missing callback argument' });
          }
          res.json(data);
        });
      } catch (e) {
        console.log(e);
        return next(e);
      }
    });
  })
});

var createManyPeople = function(arrayOfPeople, done) {
  Person.create(arrayOfPeople, {"oneOperation": true}, (err, small) => {
    if (err) return done(err);
    done(null, small);
  });
};

var timeout = 10000;
app.post('/_api/create-many-people', function(req, res, next) {
  // console.log(req.rawBody);
  Person.remove({}, function(err) {
    if(err) { return (next(err)); }
    // in case of incorrect function use wait timeout then respond
    var t = setTimeout(() => { next({message: 'timeout'}) }, timeout);
    createManyPeople(JSON.parse(req.rawBody), function(err, data) {
      clearTimeout(t);
      if(err) { return (next(err)); }
      if(!data) {
        console.log('Missing `done()` argument');
        return next({message: 'Missing callback argument'});
      }

      Person.find({}, function(err, pers){
        if(err) { return (next(err)); }
        res.json(pers);
        Person.remove().exec();
      });
    });
  });
})


app.get('/_api/create-and-save-person', (req, res) => {
  const Person = mongoose.model("Person", personSchema);
  var p;
  // p = new Person({name: "Jane Fonda", age: 84, favoriteFoods: ["vodka", "air"]});
  // p = new Person({name: "r@nd0mN4m3", age: 24, favoriteFoods: ["pizza"]});
  p = new Person({name: "Gary", age: 46, favoriteFoods: ["chicken salad"]});
  // res.json(p);
  p.save((err, data) => {
    if(err) {
      res.json(err)
    }
    else {
      res.json(data)
    }
  });
})



app.listen(PORT, () => console.log(`Listening on ${ PORT }`))
